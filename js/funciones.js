function myFunction() {
    var x, y, xx, yy;
    x = document.getElementById("ancho").value;
    y = document.getElementById("alto").value;
    xx = document.getElementById("precio").value;
    yy = document.getElementById("m2").value;
    if ((xx == "")) {
        alert("El campo Cambio $u$ no pueden quedar vacio");
        return true;
    }
    if ((x == "")) {
        alert("El campo Ancho no pueden quedar vacio");
        return true;
    }
    if ((y == "")) {
        alert("El campo Alto no pueden quedar vacio");
        return true;
    }
    if ((yy == "")) {
        alert("El campo Precio M2 no pueden quedar vacio");
        return true;
    }
    var ancho = parseFloat(x) / 1000;
    var alto = parseFloat(y) / 1000;
    var sus = parseFloat(xx) / 100;
    var suma = parseFloat(ancho) * parseFloat(alto);
    var pm2 = parseFloat(suma) * parseFloat(yy);
    var pbs = parseFloat(pm2) * parseFloat(sus);
    suma = suma.toFixed(2);
    pm2 = pm2.toFixed(2);
    pbs = pbs.toFixed(2);
    document.getElementById("sumando").innerHTML = suma;
    document.getElementById("sumandoo").innerHTML = pm2;
    document.getElementById("sumandooo").innerHTML = pbs;
}
$('.input').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});